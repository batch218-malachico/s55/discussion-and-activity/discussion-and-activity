import Banner from '../components/Banner';
import Highlights from '../components/Highlights';



export default function Home() {


	const data = {
		title: "Chicoy Pizza",
		content: "Life is uncertain. But Pizza is always a sure thing",
		destination: "/menus",
		label: "Checkout Now!"
	}

	return (
		<>
		<Banner data={data} />
		<Highlights />
		
		</>

	)
}